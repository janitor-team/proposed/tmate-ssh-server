Source: tmate-ssh-server
Section: admin
Priority: optional
Maintainer: Adrian Vondendriesch <adrian.vondendriesch@credativ.de>
Uploaders: Christoph Berg <myon@debian.org>,
Build-Depends:
 debhelper-compat (= 12),
 libevent-dev,
 libmsgpack-dev,
 libncurses-dev,
 libssh-dev,
 libutempter-dev,
 pkg-config,
 txt2man,
Standards-Version: 4.5.0
Homepage: https://github.com/tmate-io/tmate-ssh-server
Vcs-Git: https://salsa.debian.org/debian/tmate-ssh-server.git
Vcs-Browser: https://salsa.debian.org/debian/tmate-ssh-server

Package: tmate-ssh-server
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 lsb-base,
 openssh-client,
Suggests: tmate
Description: terminal multiplexer with instant terminal sharing -- server
 tmate provides an instant pairing solution, allowing you to share a terminal
 with one or several teammates. Together with a voice call, it's almost like
 pairing in person. The terminal sharing works by using SSH connections to
 backend servers running tmate-ssh-server; teammates need to be given a
 randomly-generated token to be able to join a session.
 .
 tmate is a modified version of tmux, and uses the same configurations such as
 keybindings, color schemes etc.
 .
 This package contains the tmate-ssh-server.
